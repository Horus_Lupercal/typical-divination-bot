package telegram.bot.divinationbot.botapi.handlers.menu;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import telegram.bot.divinationbot.botapi.BotState;
import telegram.bot.divinationbot.botapi.InputMessageHandler;
import telegram.bot.divinationbot.service.JokeService;
import telegram.bot.divinationbot.service.MainMenuService;
import telegram.bot.divinationbot.service.ReplyMessagesService;

@Component
public class JokeMenuHandler implements InputMessageHandler {
  private MainMenuService mainMenuService;
  private ReplyMessagesService messageService;
  private JokeService jokeService;
  
  public JokeMenuHandler(JokeService jokeService, ReplyMessagesService messageService) {
    super();
    this.jokeService = jokeService;
    this.messageService = messageService;
  }

  @Override
  public SendMessage handle(Message message) {
    return jokeService.getMainMenuMessage(message.getChatId());
  }

  @Override
  public BotState getHandlerName() {
    return BotState.SHOW_JOKE;
  }
}
