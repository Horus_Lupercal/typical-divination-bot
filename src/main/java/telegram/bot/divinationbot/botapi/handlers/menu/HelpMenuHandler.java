package telegram.bot.divinationbot.botapi.handlers.menu;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import telegram.bot.divinationbot.botapi.BotState;
import telegram.bot.divinationbot.botapi.InputMessageHandler;
import telegram.bot.divinationbot.service.MainMenuService;
import telegram.bot.divinationbot.service.ReplyMessagesService;

@Component
public class HelpMenuHandler implements InputMessageHandler {
  private MainMenuService mainMenuService;
  private ReplyMessagesService messageService;
  
  public HelpMenuHandler(MainMenuService mainMenuService, ReplyMessagesService messageService) {
    super();
    this.mainMenuService = mainMenuService;
    this.messageService = messageService;
  }

  @Override
  public SendMessage handle(Message message) {
    return mainMenuService.getMainMenuMessage(message.getChatId(),
        messageService.getReplyText("reply.showHelpMenu"));
  }

  @Override
  public BotState getHandlerName() {
    return BotState.SHOW_HELP_MENU;
  }
}
