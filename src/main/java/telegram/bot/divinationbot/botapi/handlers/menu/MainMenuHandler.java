package telegram.bot.divinationbot.botapi.handlers.menu;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import telegram.bot.divinationbot.botapi.BotState;
import telegram.bot.divinationbot.botapi.InputMessageHandler;
import telegram.bot.divinationbot.service.MainMenuService;
import telegram.bot.divinationbot.service.ReplyMessagesService;

@Component
public class MainMenuHandler implements InputMessageHandler {
  private ReplyMessagesService messagesService;
  private MainMenuService mainMenuService;

  public MainMenuHandler(ReplyMessagesService messagesService, MainMenuService mainMenuService) {
    this.messagesService = messagesService;
    this.mainMenuService = mainMenuService;
  }

  @Override
  public SendMessage handle(Message message) {
    return mainMenuService.getMainMenuMessage(message.getChatId(),
        messagesService.getReplyText("reply.showMainMenu"));
  }
  
  @Override
  public BotState getHandlerName() {
      return BotState.SHOW_MAIN_MENU;
  }
}
