package telegram.bot.divinationbot.service;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Service
public class JokeService {
  
  public SendMessage getMainMenuMessage(final long chatId) {
    final SendMessage sendMessage = new SendMessage();
    sendMessage.enableMarkdown(true);
    sendMessage.setChatId(chatId);
    sendMessage.setText("здесь могла быть ваша шутка!");
    return sendMessage;
  }

}
