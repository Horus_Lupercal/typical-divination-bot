package telegram.bot.divinationbot.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;
import telegram.bot.divinationbot.DivinationBot;

@RestController
public class Controller {
  private final DivinationBot telegramBot;

  public Controller(DivinationBot telegramBot) {
      this.telegramBot = telegramBot;
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  public BotApiMethod<?> onUpdateReceived(@RequestBody Update update) {
      return telegramBot.onWebhookUpdateReceived(update);
  }
}
