package telegram.bot.divinationbot.utils;

import lombok.AllArgsConstructor;
import com.vdurmont.emoji.EmojiParser;

@AllArgsConstructor
public enum Emojis {
  SPARKLES(EmojiParser.parseToUnicode(":sparkles:")),
  SCROLL(EmojiParser.parseToUnicode(":scroll:")),
  MAGE(EmojiParser.parseToUnicode(":mage:"));

  private String emojiName;

  @Override
  public String toString() {
      return emojiName;
  }
}
