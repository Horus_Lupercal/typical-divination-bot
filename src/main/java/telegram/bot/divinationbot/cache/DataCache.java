package telegram.bot.divinationbot.cache;

import telegram.bot.divinationbot.botapi.BotState;
import telegram.bot.divinationbot.model.UserProfileData;

public interface DataCache {
  void setUsersCurrentBotState(int userId, BotState botState);

  BotState getUsersCurrentBotState(int userId);

  UserProfileData getUserProfileData(int userId);

  void saveUserProfileData(int userId, UserProfileData userProfileData);
}
