package telegram.bot.divinationbot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import lombok.Getter;
import lombok.Setter;
import telegram.bot.divinationbot.DivinationBot;
import telegram.bot.divinationbot.botapi.TelegramFacade;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "telegrambot")
public class BotConfig {
  private String webHookPath;
  private String botUserName;
  private String botToken;

  @Bean
  public DivinationBot divinationBot(TelegramFacade telegramFacade) {
    DivinationBot bot = new DivinationBot(telegramFacade);
    bot.setBotUserName(botUserName);
    bot.setBotToken(botToken);
    bot.setWebHookPath(webHookPath);

    return bot;
  }

  @Bean
  public MessageSource messageSource() {
    ReloadableResourceBundleMessageSource messageSource =
        new ReloadableResourceBundleMessageSource();

    messageSource.setBasename("classpath:messages");
    messageSource.setDefaultEncoding("UTF-8");
    return messageSource;
  }
}
